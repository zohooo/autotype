﻿
/*
代码功能：中文 LaTeX 文档输入时自动切换中英文
代码仓库：https://bitbucket.org/zohooo/autotype
当前版本：0.2
*/

;; 总是可用Shift键切换中英文输入法
Shift:: toggleIme()

;; 在中文模式中按$自动切换为英文模式
:*:$::
if !isDefaultIme() {
  setLayout()
}
Send $
return

;; 在中文模式中按\自动切换为英文模式
:*:\::
if !isDefaultIme() {
  setLayout()
}
Send \
return

;; 在中文模式中总是使用实心的中文句号
:*:.::
if (isDefaultLang() && !isDefaultIme()) {
  SendInput ．
} else {
  Send .
}
return

;; 从当前的键盘布局代码中判断输入语言代码
;; 输入语言代码为4位16进制数
;; 16进制：美国英语 0x0409，简体中文 0x0804，台湾繁体 0x0404，香港繁体 0x0c04
;; 10进制：美国英语  1033， 简体中文  2052， 台湾繁体  1028， 香港繁体  3076
;; 键盘布局代码前面还多4位16进制数
;; 美国英语：QWERTY布局 0x04090409，DVORAK布局：0xF0020409
;; 简体中文：美式键盘 0x00000804，智能ABC 0xE0040804，QQ拼音 0xE0230804，微软拼音 0xE0240804

isDefaultLang() {
  layout := GetLayout()
  lang := Mod(layout, 16**4) ;得到后四位
  return (lang = 0x0804)
}

isDefaultIme() {
  layout := GetLayout()
  return (layout = 0x08040804) ;自带中文输入法似乎也是此值，暂时不管
}

GetLayout() {
  hwnd := WinExist("A") ;当前活跃窗口
  id := DllCall("GetWindowThreadProcessId", UInt, hwnd)
  layout := DllCall("GetKeyboardLayout", UInt, id, UInt)
  return layout
}

SetLayout() {
  hkl := DllCall("LoadKeyboardLayout", Str, "00000804", UInt, "257")
  ControlGetFocus, ctl, A
  SendMessage, 0x50, 0, hkl, %ctl%, A ;; 0x0050 = WM_INPUTLANGCHANGEREQUEST
}

toggleIme() {
  Send ^{Space}
}
